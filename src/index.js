import React from 'react';
import ReactDOM from 'react-dom';
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp';
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker';

mapboxgl.workerClass = MapboxWorker;
mapboxgl.accessToken = 'pk.eyJ1Ijoia2F5dGx5bmd1ZXJyZXJvIiwiYSI6ImNrbGd5ejMwMTB5cDQyd29paGZoYnE0dW4ifQ.FN8hXcvlYPaxkl0UdIatVQ';

// this data is used to add the markers
// testing push
const vet = [
  {
  "location": "Veteran Affairs",
  "city": "Austin",
  "state": "Texas",
  "coordinates": [-97.747356, 30.220998]
  },
  {
  "location": "Austin Vet Care at Central Park",
  "city": "Austin",
  "state": "Texas",
  "coordinates": [-97.740619, 30.309429]
  },
  {
    "location": "Capital Veterinary Clinic",
    "city": "Austin",
    "state": "Texas",
    "coordinates": [-97.77002, 30.213948]
    },
    {
    "location": "Riverside Veterinary Clinic",
    "city": "Austin",
    "state": "Texas",
    "coordinates": [-97.729604, 30.229356]
      },
      {
        "location": "Burnet Road Animal Hospital",
        "city": "Austin",
        "state": "Texas",
        "coordinates": [-97.727867, 30.366769]
        }

]



// this defines ' Map ' 
// These are default states for the app to use... so when the initial map is rendered it will be centered at given coords
// the lng and lat are those of Austin, Tx
class Map extends React.PureComponent {
  constructor(props) {
    super(props); 
    this.state = { 
      lng: -97.7431,
      lat: 30.2672,
      zoom: 12
    };
    this.mapContainer = React.createRef();
  }

  // This is essentially initializing the map
  // this is a 'lifecycle method' vs using hooks
  componentDidMount() {
    const { lng, lat, zoom } = this.state;
    const map = new mapboxgl.Map({
      container: this.mapContainer.current, // container option tells tells mapbox GL JS to render the map inside a specific DOM element.
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });

    // this for each function is reading the data variable and adding them on to the map as a marker
    vet.forEach((location) => {
      console.log(location)
      var marker = new mapboxgl.Marker()
      .setLngLat(location.coordinates)
      .setPopup(new mapboxgl.Popup({ offset: 30 })
      .setHTML('<h4>' + location.city + '</h4>' + location.location))
      .addTo(map)
    })


    // This geocoder will allow a user to input an address to retrieve a route from 
    var geocoder = new MapboxGeocoder({ 
        accessToken: mapboxgl.accessToken, 
        mapboxgl: mapboxgl, 
        marker: {
          color: 'blue'
        }, 
      });
      map.addControl(geocoder);
      
      
    // this variable is the start coordinates of where the initial marker will be at on the map when it renders  
      var start = [-97.7526, 30.2289];
    // create a function to make a directions request
    // This is the function that makes the request and draws the route
    function getRoute(end) {
        var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;
      // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
      //  You can retrieve data from a URL without having to do a full page refresh. This enables a Web page to update just part of a page without disrupting what the user is doing
        var req = new XMLHttpRequest(); 
        req.open('GET', url, true);
        req.onload = function() {
          var json = JSON.parse(req.response);
          var data = json.routes[0];
          var route = data.geometry.coordinates;
          var geojson = {
            type: 'Feature',
            properties: {},
            geometry: {
                type: 'LineString',
                coordinates: route
            }
        };
        // if the route already exists on the map, reset it using setData
        if (map.getSource('route')) {
            map.getSource('route').setData(geojson);
        } 
        else { // otherwise, make a new request
            map.addLayer({
                id: 'route',
                type: 'line',
                source: {
                    type: 'geojson',
                    data: {
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: 'LineString',
                            coordinates: geojson
                        }
                }
            },
            layout: {
                'line-join': 'round',
                'line-cap': 'round'
            },
            paint: {
                'line-color': 'black',
                'line-width': 5,
                'line-opacity': 0.75
            }
            });
        }
        // add turn instructions


        // get the sidebar and add the instructions
var instructions = document.getElementById('instructions');
var steps = data.legs[0].steps;

    var tripInstructions = [];
    for (var i = 0; i < steps.length; i++) {
      tripInstructions.push('<br><li>' + steps[i].maneuver.instruction) + '</li>';
      instructions.innerHTML = '<br><span class="duration">Trip duration: ' + Math.floor(data.duration / 60) + ' min 🚘 </span>' + tripInstructions;
    }
      };
        req.send();
    }
    
    map.on('load', function() {
        // make an initial directions request that
        // starts and ends at the same location
        getRoute(start);
//      adding layers to the map
        map.addLayer({
        id: 'point',
        type: 'circle',
        source: {
            type: 'geojson',
            data: {
              type: 'FeatureCollection',
              features: [{
                type: 'Feature',
                properties: {},
                geometry: {
                type: 'Point',
                coordinates: start
                }
            }
        ]
    }
        },
        paint: {
            'circle-radius': 10,
            'circle-color': '#3887be'
        }
        });
        map.addSource('end-point', {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: []
            }
        });

        map.addLayer({
            id: 'end',
            source: 'end-point',
            type: 'circle',
            paint: {
                'circle-radius': 6,
                'circle-color': 'purple'
            },
            layout: {
                visibility: 'none'
             }
  });
  // on search result, update map with route
  geocoder.on('result', function (e) {
    map.getSource('end-point').setData(e.result.geometry);
    var end = e.result.geometry.coordinates;
    getRoute(end);
  });
});
}
  // the mapContainer ref specifies that map should be drawn to the HTML page in a new <div> element
  render() {
    const { lng, lat, zoom } = this.state;
    return (
      <div>
        {/* <div className="sidebar">
          Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
        </div>  */}
        <div ref={this.mapContainer} className="map-container" /> 
      </div>
    );
  }
}
// this specifies that it should be rendered in the 
ReactDOM.render(<Map />, document.getElementById('app'));
